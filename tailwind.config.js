/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      backgroundImage: (theme) => ({
        game: "url('/src/images/background.png')",
        logo: "url('/src/images/logo.png')",
        progress: "url('/src/images/load-bg.png')",
        progressFill: "url('/src/images/load.png')",
      }),
      screens: {
        'tablet': '640px',
        'laptop': '1024px',
        'desktop': '1280px',
      }
    },
  },
  plugins: [],
}

