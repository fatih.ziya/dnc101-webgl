module.exports = function(app) {
    app.use((req, res, next) => {
        if(req.originalUrl.indexOf(".br")>-1){
            res.set({
                'Content-Encoding': 'br'
            });
        }
        // console.log(res);

        next();
    }); 
  };