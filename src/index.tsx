import "./index.scss";

import * as Sentry from "@sentry/react";

import App, { APP_GAME_STATUS_QUERY } from "./App";

import { AppGetOkey101GameStatusQuery } from "./__generated__/AppGetOkey101GameStatusQuery.graphql";
import React from "react";
import ReactDOM from "react-dom/client";
import { RelayEnvironment } from "./RelayEnvironment";
import { RelayEnvironmentProvider } from "react-relay";
import { loadQuery } from 'react-relay';
import reportWebVitals from "./reportWebVitals";

const _window = (window as any);
const envConfig = _window.__ENV_DATA__;

Sentry.init({
  dsn: envConfig.sentryDsn,
  integrations: [new Sentry.BrowserTracing(), new Sentry.Replay(), new Sentry.Integrations.Breadcrumbs({ console: false })],
  tracesSampleRate: 1.0, // Capture 100% of the transactions, reduce in production!
  replaysSessionSampleRate: 0.1, // This sets the sample rate at 10%. You may want to change it to 100% while in development and then sample at a lower rate in production.
  replaysOnErrorSampleRate: 1.0, // If you're not already sampling the entire session, change the sample rate to 100% when sampling sessions where errors occur.
});

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
const rootQueryRef= loadQuery<AppGetOkey101GameStatusQuery>(
  RelayEnvironment,
  APP_GAME_STATUS_QUERY, {}
);
root.render(
  <RelayEnvironmentProvider environment={RelayEnvironment}>
    <React.StrictMode>
      <App rootQueryRef={rootQueryRef}/>
    </React.StrictMode>
  </RelayEnvironmentProvider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
