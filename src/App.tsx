import './App.css';

import { PreloadedQuery, usePreloadedQuery } from 'react-relay';

import { AppGetOkey101GameStatusQuery } from './__generated__/AppGetOkey101GameStatusQuery.graphql';
import { Helmet } from "react-helmet";
import Okey101Game from './components/Okey101Game';
import graphql from 'babel-plugin-relay/macro';
import { isWebGL2Supported } from 'webgl-detector';
import queryString from 'query-string';
import { useCookies } from 'react-cookie';
import { useEffect } from 'react';

export const APP_GAME_STATUS_QUERY = graphql`
query AppGetOkey101GameStatusQuery {
  getOkey101GameStatus {
    isOnline
    title
    description
  }
}

`;




interface AppProps {
  rootQueryRef: PreloadedQuery<AppGetOkey101GameStatusQuery, {}>
}
function App(Props: AppProps) {
  const _window = (window as any);
  const envConfig = _window.__ENV_DATA__;
  const gameStatusQuery = usePreloadedQuery<AppGetOkey101GameStatusQuery>(APP_GAME_STATUS_QUERY, Props.rootQueryRef);
  const [, setCookie] = useCookies(['referenceCode']);
  const parsedQueryString = queryString.parse(window.location.search);
  if (parsedQueryString.refCode && parsedQueryString.refCode.length >= 6) {
    setCookie("referenceCode", parsedQueryString.refCode, { path: '/' });
    window.location.replace(window.location.origin + window.location.pathname);
  }

  return (
    <>
      <Helmet>
        <title>{envConfig.title}</title>
      </Helmet>
      {isWebGL2Supported() ? (
          <Okey101Game />) : (
        <div className="fill-window">
          <p style={{ textAlign: "center", fontSize: '2rem', color: "#fff" }}>WenGL2 desteği olmayan cihazlara (Eski telefonlara) destek verilmemektedir.</p>
        </div>
      )}
    </>

  );
}

export default App;
