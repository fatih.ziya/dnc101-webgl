import { Fragment, useCallback, useEffect, useRef, useState } from "react";
import { FullScreen, useFullScreenHandle } from "react-full-screen";
import { Unity, useUnityContext } from "react-unity-webgl";
import { isIOS, isMobile, isSafari, isTablet } from 'react-device-detect';

//@ts-ignore
import TawkMessengerReact from '@tawk.to/tawk-messenger-react';
import { useCookies } from "react-cookie";
import { useMediaQuery } from 'react-responsive'
import { usePageVisibility } from 'react-page-visibility';
import {
    useWindowSize,
} from '@react-hook/window-size'

export enum GameAuthEventType {
    LOGIN = 0,
    LOGOUT = 1,
}
export enum GameActionEventType {
    CONNECT = 0,
    LEAVE = 1,
}

function Okey101Game() {
    const _window = (window as any);
    const initialData = _window.__INITIAL_DATA__;
    const tawkMessengerRef = useRef();
    const isPortrait = useMediaQuery({ query: '(orientation: portrait)' })
    const isPageVisible = usePageVisibility()
    const fullScreenHandle = useFullScreenHandle();
    const [cookies] = useCookies(['referenceCode']);
    const [width, height] = useWindowSize()
    function handleCacheControl(url: string) {
        // if (url.match(/\.data/) || url.match(/\.bundle/)) {
        //     return "must-revalidate";
        // }
        // return "no-store";
        return "must-revalidate";
    }

    const { unityProvider, requestFullscreen, loadingProgression, isLoaded, addEventListener, removeEventListener, sendMessage, unload } = useUnityContext({
        loaderUrl: "Build/" + initialData.LOADER_FILENAME + "?v=" + initialData.PRODUCT_VERSION,
        dataUrl: "Build/" + initialData.DATA_FILENAME + "?v=" + initialData.PRODUCT_VERSION,
        frameworkUrl: "Build/" + initialData.FRAMEWORK_FILENAME + "?v=" + initialData.PRODUCT_VERSION,
        codeUrl: "Build/" + initialData.CODE_FILENAME + "?v=" + initialData.PRODUCT_VERSION,
        companyName:"TSC",
        productName:"TSC_OKEY",
        productVersion:initialData.PRODUCT_VERSION,
        cacheControl: handleCacheControl,
        webglContextAttributes: {
            alpha: true,
            antialias: true,
            depth: true,
            failIfMajorPerformanceCaveat: true,
            powerPreference: "high-performance",
            premultipliedAlpha: false,
            preserveDrawingBuffer: false,
            stencil: false,
            desynchronized: true,
            xrCompatible: false,
        },
    });
    const handleAuthEvent = useCallback((eventType: GameAuthEventType, playerJson: string) => {
        if(eventType === GameAuthEventType.LOGIN){

            const player = JSON.parse(playerJson);
            //@ts-ignore
            tawkMessengerRef.current.visitor = {
                id : player.Id,
                name : player.DisplayName,
            };
        }else{
            //@ts-ignore
            tawkMessengerRef.current.visitor = {
                id : "",
                name : "",
            };
        }

    }, []);

    const handleGameEvent = useCallback((eventType: GameActionEventType, roomJson: string) => {
        if(eventType === GameActionEventType.CONNECT){
            const room = JSON.parse(roomJson);
            //@ts-ignore
            tawkMessengerRef.current.visitor = {
                roomId : room.RoomId,
                tableLimit : room.TableLimit,
            };
        }else{
            //@ts-ignore
            tawkMessengerRef.current.visitor = {
                roomId : "",
                tableLimit : "",
            };
        }
    }, []);


    const toggleLiveSupportEvent = useCallback(() => {

        console.log("toggleLiveSupportEvent");
        if (tawkMessengerRef.current) {
            fullScreenHandle.exit();
            //@ts-ignore
            tawkMessengerRef.current.maximize();
        }

    }, []);

    useEffect(() => {
        addEventListener("GameDispatcher", handleGameEvent);
        addEventListener("AuthDispatcher", handleAuthEvent);
        addEventListener("ToggleLiveSupportDispatcher", toggleLiveSupportEvent);
        return () => {
            removeEventListener("GameDispatcher", handleGameEvent);
            removeEventListener("AuthDispatcher", handleAuthEvent);
            removeEventListener("ToggleLiveSupportDispatcher", toggleLiveSupportEvent);
        };
    }, [addEventListener, removeEventListener, handleGameEvent, handleAuthEvent, toggleLiveSupportEvent]);


    
    useEffect(() => {
        if (isLoaded) {
            sendMessage("WebglController", "DeviceType", JSON.stringify({
                IsIOS: isIOS,
                IsMobile: isMobile,
                IsSafari: isSafari,
                IsTablet: isTablet,
                PartnerReferenceCode: cookies.referenceCode??""
            }));
        }
    }, [isLoaded, cookies, sendMessage]);

    useEffect(() => {
        if (isLoaded) {
            sendMessage("WebglController", "PageVisibility", JSON.stringify({
                IsPageVisible: isPageVisible,

            }));
        }
    }, [isLoaded, isPageVisible, sendMessage]);


    useEffect(() => {
        if (isLoaded) {
            sendMessage("WebglController", "DeviceOrientation", JSON.stringify({
                IsPortrait: isPortrait,
                IsLandscape: !isPortrait,
                Orientation: isPortrait ? "portrait" : "landscape",
                ScreenWidth: width,
                ScreenHeight: height,
            }));
        }
    }, [isLoaded, isPortrait, width, height, sendMessage]);

    const onUnreadCountChanged = (count:number) => {
       
    }


    return (
        <div id="main-container" className="fill-window">
            <FullScreen handle={fullScreenHandle} className="unity-container">
                <Fragment>
                    <Unity

                        devicePixelRatio={window.devicePixelRatio}
                        unityProvider={unityProvider}

                        style={{ width: width, height: height }}
                        className="unity-canvas"

                    ></Unity>
                    <div className={fullScreenHandle.active ? "button floatbottomLeft fullscreenOFF" : "button floatbottomLeft fullscreenON"} style={{ display: !isIOS ? "block" : "none" }} onClick={fullScreenHandle.active ? fullScreenHandle.exit : fullScreenHandle.enter}>
                        <span className='imgbutton' />

                    </div>
                    <div id="tawk-embed"></div>

                    <TawkMessengerReact
                        propertyId="6476f83aad80445890f020b6"
                        widgetId="1h1obj96d"
                        ref={tawkMessengerRef}
                        onUnreadCountChanged={onUnreadCountChanged}/>
                </Fragment>

            </FullScreen>





            <div id="loader" className="loader" style={{ display: !isLoaded ? "block" : "none" }}>
                <div className="container">
                    <div className="center">
                        <div className="logo"></div>
                    </div>

                    <div className="loading">
                        <div className="progressbar">
                            <div id="fill" className="fill" style={{ width: (loadingProgression * 100) + "%" }}></div>
                        </div>
                        <h3>Yükleniyor...</h3>
                    </div>
                </div>


                <div className="footer-cont">
                    <p><img src="/images/tsc_games_logo.png" width="100" /></p>
                </div>

            </div>
        </div>

    );

}

export default Okey101Game;