/**
 * @generated SignedSource<<9f72d59d8a479a90f1e6d863d8a13764>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type AppGetOkey101GameStatusQuery$variables = {};
export type AppGetOkey101GameStatusQuery$data = {
  readonly getOkey101GameStatus: {
    readonly description: string;
    readonly isOnline: boolean;
    readonly title: string;
  } | null;
};
export type AppGetOkey101GameStatusQuery = {
  response: AppGetOkey101GameStatusQuery$data;
  variables: AppGetOkey101GameStatusQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "GameStatusResponse",
    "kind": "LinkedField",
    "name": "getOkey101GameStatus",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "isOnline",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "title",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "description",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "AppGetOkey101GameStatusQuery",
    "selections": (v0/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "AppGetOkey101GameStatusQuery",
    "selections": (v0/*: any*/)
  },
  "params": {
    "cacheID": "5b9886494da07f3b4083f21a171f1a5d",
    "id": null,
    "metadata": {},
    "name": "AppGetOkey101GameStatusQuery",
    "operationKind": "query",
    "text": "query AppGetOkey101GameStatusQuery {\n  getOkey101GameStatus {\n    isOnline\n    title\n    description\n  }\n}\n"
  }
};
})();

(node as any).hash = "19429cf44b24a1ac7b2f156cd819d83b";

export default node;
