'use strict';

const express = require('express');
const http = require('http');
const path = require('path');

// Constants
const PORT = 3000;
const HOST = '0.0.0.0';

// App
const app = express();
app.use((req, res, next) => {
    // if(req.originalUrl.indexOf(".js.br")>-1){
    //     console.log(".js.br");
    //     res.set('Content-Encoding', 'br');
    //     res.set('Content-Type', 'text/javascript');
    // }else if(req.originalUrl.indexOf(".data.br")>-1){
    //     console.log(".data.br");
    //     res.set('Content-Encoding', 'br');
    //     res.set('Content-Type', 'application/octet-stream');
    // }else if(req.originalUrl.indexOf(".wasm.br")>-1){
    //     console.log(".wasm.br");
    //     res.set('Content-Encoding', 'br');
    //     res.set('Content-Type', 'application/wasm');
    // }
    next();
}); 
app.use(express.static(path.join(__dirname, '/')));
// Send html on '/'path
app.get('/', (req, res) => {
res.sendFile(path.join(__dirname, + '/index.html'));
})

http.createServer(app).listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);